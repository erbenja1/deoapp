#Setup
```
Required
 npm version ^6.2.0 
Prepare dist
  commands
    npm install
    npm build
  Folder dist has all needed content ready to be hosted on web server.
```




# deoapp-frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
