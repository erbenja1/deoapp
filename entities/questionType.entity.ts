
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { QuestionEntity } from './question.entity';

@Entity('questionType')
export class QuestionTypeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: string;  

  @Column({default: false})
  serverEvaluation: boolean;

  @OneToMany(type => QuestionEntity, question => question.type)
  questions: QuestionEntity[];
}