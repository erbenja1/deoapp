
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { TestEntity } from './test.entity';

@Entity('category')
export class CategoryEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string; 
  
  @Column()
  classMax: number;

  @Column()
  classMin: number;

  @OneToMany(type => TestEntity, test => test.category)
  tests: TestEntity[];

}