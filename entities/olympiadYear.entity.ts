
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { OlympiadRoundEntity } from './olympiadRound.entity';

@Entity('olympiadYear')
export class OlympiadYearEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  year: number;

  @Column()
  description: string;

  @Column({default: false})
  public: boolean;

  @Column({type: 'date'})
  registrationDeadline: any;

  @OneToMany(type => OlympiadRoundEntity, round => round.year)
  rounds: OlympiadRoundEntity[];

}