
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { PermissionEntity } from './permission.entity';

@Entity('account')
export class AccountEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column()
  email: string;

  @Column({ nullable: true })
  firstname: string;

  @Column({ nullable: true })
  lastname: string;

  @OneToMany(type => PermissionEntity, permission => permission.account)
  permissions: PermissionEntity[];

}