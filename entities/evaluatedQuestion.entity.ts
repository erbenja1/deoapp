
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne } from 'typeorm';
import { AccessCodeEntity } from './accessCode.entity';
import { AnsweredQuestionEntity } from './answeredQuestion.entity';
import { AccountEntity } from './account.entity';

@Entity('evaluatedQuestion')
export class EvaluatedQuestionEntity {
  @PrimaryGeneratedColumn()
  id: number;
  
  @Column()
  points: number;

  @Column()
  note: string;

  @Column({default: false})
  closed: boolean;

  @Column({type: "timestamp with time zone"})
  lastModified: any;

  @ManyToOne(type => AccountEntity)
  evaluator: AccessCodeEntity

  @OneToOne(type => AnsweredQuestionEntity, answeredQuestion => answeredQuestion.evaluation)
  answeredQuestion: AnsweredQuestionEntity;

}