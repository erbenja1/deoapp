
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany } from 'typeorm';
import { AccountEntity } from './account.entity';
import { PostTagEntity } from './postTag.entity';

@Entity('post')
export class PostEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string; 

  @Column()
  content: string; 

  @Column({type: "timestamp with time zone"})
  lastModified: any;

  @Column({type: "timestamp with time zone"})
  created: any;

  @ManyToOne(type => AccountEntity)
  author: AccountEntity;

  @ManyToMany(type => PostTagEntity, tag => tag.posts)
  tags: PostTagEntity[];

}