
import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from 'typeorm';
import { PostEntity } from './post.entity';

@Entity('postTag')
export class PostTagEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string; 

  @ManyToMany(type => PostEntity, post => post.tags)
  posts: PostEntity[];

}