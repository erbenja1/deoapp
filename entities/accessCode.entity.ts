
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';
import { AnsweredQuestionEntity } from './answeredQuestion.entity';
import { OlympiadRoundEntity } from './olympiadRound.entity';
import { ContestantEntity } from './contestant.entity';

@Entity('accessCode')
export class AccessCodeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  code: string;

  @Column({type: "timestamp with time zone", default: null})
  activated: any;

  @Column({type: "timestamp with time zone"})
  testStart: any;

  @ManyToOne(type => ContestantEntity, contestant => contestant.accessCodes)
  contestant: ContestantEntity;

  @ManyToOne(type => OlympiadRoundEntity, round => round.accessCodes)
  round: OlympiadRoundEntity;

  @OneToMany(type => AnsweredQuestionEntity, answeredQuestion => answeredQuestion.accessCode)
  answeredQuestions: AnsweredQuestionEntity[];

}