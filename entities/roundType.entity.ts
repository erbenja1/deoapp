
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany } from 'typeorm';
import { GuaranteeEntity } from './guarantee.entity';
import { OlympiadRoundEntity } from './olympiadRound.entity';

@Entity('roundType')
export class RoundTypeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string; 

  @OneToMany(type => OlympiadRoundEntity, olympiadRound => olympiadRound.type)
  olympiadRounds: OlympiadRoundEntity[];

  @ManyToMany(type => GuaranteeEntity, guarantee => guarantee.roundTypes)
  guarantees: GuaranteeEntity[];

}