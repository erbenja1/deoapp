
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { QuestionEntity } from './question.entity';

@Entity('questionOption')
export class QuestionOptionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  content: string; 

  @Column()
  correct: number;

  @ManyToOne(type => QuestionEntity, question => question.options)
  question: QuestionEntity;
}