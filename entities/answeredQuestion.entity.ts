
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne } from 'typeorm';
import { AccessCodeEntity } from './accessCode.entity';
import { QuestionEntity } from './question.entity';
import { EvaluatedQuestionEntity } from './evaluatedQuestion.entity';

@Entity('answeredQuestion')
export class AnsweredQuestionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: "json"})
  answer: any;  

  @Column({type: "timestamp with time zone"})
  lastModified: any;

  @Column({type: "timestamp with time zone"})
  created: any;

  @ManyToOne(type => AccessCodeEntity, accessCode => accessCode.answeredQuestions)
  accessCode: AccessCodeEntity;

  @ManyToOne(type => QuestionEntity, question => question.answeredQuestions)
  question: QuestionEntity;

  @OneToOne(type => EvaluatedQuestionEntity, evaluatedQuestion => evaluatedQuestion.answeredQuestion)
  evaluation: EvaluatedQuestionEntity;

}