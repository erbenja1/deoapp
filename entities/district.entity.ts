
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany } from 'typeorm';
import { SchoolEntity } from './school.entity';
import { GuaranteeEntity } from './guarantee.entity';
import { RegionEntity } from './region.entity';

@Entity('district')
export class DistrictEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string; 

  @ManyToOne(type => RegionEntity, region => region.districts)
  region: RegionEntity;

  @OneToMany(type => SchoolEntity, school => school.district)
  schools: SchoolEntity[];

  @ManyToMany(type => GuaranteeEntity, guarantee => guarantee.districts)
  guarantees: GuaranteeEntity[];

}