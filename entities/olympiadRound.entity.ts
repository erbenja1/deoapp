
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, JoinTable, OneToOne } from 'typeorm';
import { GuaranteeEntity } from './guarantee.entity';
import { RoundTypeEntity } from './roundType.entity';
import { AccessCodeEntity } from './accessCode.entity';
import { OlympiadYearEntity } from './olympiadYear.entity';
import { TestEntity } from './test.entity';

@Entity('olympiadRound')
export class OlympiadRoundEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'date'})
  roundStart: Date;

  @Column({type: 'date'})
  roundEnd: Date;

  @OneToOne(type => OlympiadRoundEntity)
  nextRound: OlympiadRoundEntity;

  @ManyToOne(type => RoundTypeEntity, roundType => roundType.olympiadRounds)
  type: RoundTypeEntity;

  @ManyToOne(type => OlympiadYearEntity, year => year.rounds)
  year: OlympiadRoundEntity;

  @OneToMany(type => TestEntity, test => test.round)
  tests: TestEntity[];

  @OneToMany(type => AccessCodeEntity, accessCode => accessCode.round)
  accessCodes: AccessCodeEntity[];

  @ManyToMany(type => GuaranteeEntity, guarantee => guarantee.guaranteedRounds)
  @JoinTable()
  guarantees: GuaranteeEntity[];

}