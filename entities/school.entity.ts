
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany } from 'typeorm';
import { ContestantEntity } from './contestant.entity';
import { GuaranteeEntity } from './guarantee.entity';
import { DistrictEntity } from './district.entity';

@Entity('school')
export class SchoolEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string; 

  @ManyToOne(type => DistrictEntity, district => district.schools)
  district: DistrictEntity;

  @OneToMany(type => ContestantEntity, contestant => contestant.school)
  contestants: ContestantEntity[];

  @ManyToMany(type => GuaranteeEntity, guarantee => guarantee.schools)
  guarantees: GuaranteeEntity[];

}