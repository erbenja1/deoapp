
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { AccountEntity } from './account.entity';

@Entity('permission')
export class PermissionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: string;  

  @ManyToOne(type => AccountEntity, account => account.permissions)
  account: AccountEntity;

}