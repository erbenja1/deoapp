
import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, OneToMany } from 'typeorm';
import { DistrictEntity } from './district.entity';
import { GuaranteeEntity } from './guarantee.entity';

@Entity('region')
export class RegionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string; 

  @OneToMany(type => DistrictEntity, district => district.region)
  districts: DistrictEntity[];

  @ManyToMany(type => GuaranteeEntity, guarantee => guarantee.regions)
  guarantees: GuaranteeEntity[];
}