
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { OlympiadRoundEntity } from './olympiadRound.entity';
import { QuestionEntity } from './question.entity';
import { CategoryEntity } from './category.entity';

@Entity('test')
export class TestEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  timeLimit: number;

  @Column({default: false})
  closed: boolean; 

  @Column({type: "timestamp with time zone"})
  lastModified: any;

  @Column({type: "timestamp with time zone"})
  created: any;

  @ManyToOne(type => OlympiadRoundEntity, round => round.tests)
  round: OlympiadRoundEntity;

  @ManyToOne(type => CategoryEntity)
  category: CategoryEntity;

  @OneToMany(type => QuestionEntity, question => question.test)
  questions: QuestionEntity[]

}