
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, JoinTable, OneToOne } from 'typeorm';
import { SchoolEntity } from './school.entity';
import { DistrictEntity } from './district.entity';
import { AccountEntity } from './account.entity';
import { RegionEntity } from './region.entity';
import { RoundTypeEntity } from './roundType.entity';
import { OlympiadRoundEntity } from './olympiadRound.entity';

@Entity('guarantee')
export class GuaranteeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(type => AccountEntity)
  account: AccountEntity;

  @ManyToMany(type => RegionEntity, region => region.guarantees)
  @JoinTable()
  regions: RegionEntity[];

  @ManyToMany(type => DistrictEntity, district => district.guarantees)
  @JoinTable()
  districts: DistrictEntity[];

  @ManyToMany(type => SchoolEntity, school => school.guarantees)
  @JoinTable()
  schools: SchoolEntity[];

  @ManyToOne(type => RoundTypeEntity, roundType => roundType.guarantees)
  roundTypes: RoundTypeEntity[];

  @ManyToMany(type => OlympiadRoundEntity, round => round.guarantees)
  guaranteedRounds: OlympiadRoundEntity[];
}