
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { AnsweredQuestionEntity } from './answeredQuestion.entity';
import { QuestionTypeEntity } from './questionType.entity';
import { TestEntity } from './test.entity';
import { QuestionOptionEntity } from './questionOption.entity';

@Entity('question')
export class QuestionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  task: string; 
  
  @Column()
  points: number;

  @Column()
  orderNum: number;

  @Column({type: "timestamp with time zone"})
  lastModified: any;

  @Column({type: "timestamp with time zone"})
  created: any;

  @OneToMany(type => QuestionOptionEntity, questionOption => questionOption.question)
  options: QuestionOptionEntity[];

  @ManyToOne(type => QuestionTypeEntity, questionType => questionType.questions)
  type: QuestionTypeEntity;

  @OneToMany(type => AnsweredQuestionEntity, answeredQuestion => answeredQuestion.question)
  answeredQuestions: AnsweredQuestionEntity[];

  @ManyToOne(type => TestEntity, test => test.questions)
  test: TestEntity;

}