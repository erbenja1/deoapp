
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { AccessCodeEntity } from './accessCode.entity';
import { SchoolEntity } from './school.entity';

@Entity('contestant')
export class ContestantEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: "json"})
  firstname: string; 
  
  @Column()
  lastname: string;

  @Column()
  email: string;

  @Column("date")
  birthdate: Date;

  @Column()
  class: number;

  @ManyToOne(type => SchoolEntity, school => school.contestants)
  school: SchoolEntity;

  @OneToMany(type => AccessCodeEntity, accessCode => accessCode.contestant)
  accessCodes: AccessCodeEntity[];
}